// Receives a JSON object and checks if there is any null or empty attribute
const checkObj = (jsonObj) => {
    for (var attribute in jsonObj) {
        if (jsonObj[attribute] !== null || jsonObj[attribute] !== "") {
            return false;
        }
        return true;
    };
};

// Check if the pin has the right length 
const pinLength = (pin, rightLength) => {
    if (pin.toString().length === rightLength) {
        return true;
    }
    return false;
};

//Verify that user is 18+
const verifyAge = (birthday) => {
    let date = new Date(birthday);
    let now = new Date();
    let age = now.getFullYear() - date.getFullYear();
    if (age > 18) {
        return true;
    }
    if (age === 18 || age - 18 === 1) {
        if (date.getMonth() < now.getMonth()) {
            return true;
        }
        if (date.getMonth() === now.getMonth() && date.getDate() <= now.getDate()) {
            return true;
        }
    }
    return false;
};

module.exports = {
    checkObj,
    pinLength,
    verifyAge
};