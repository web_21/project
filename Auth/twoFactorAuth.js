const key = require('../secrets/messagebirdKey.json'); //secret key
const messagebird = require('messagebird')(key['messagebird_key']);

const sendSMS = (phoneNumber, code) => {
    //SMS structure with phone number and verification code
    const params = {
        'originator': 'TubeKids',
        'recipients': [
            `+506${phoneNumber}`
        ],
        'body': `${code} is your TubeKids verification code.`
    };

    //Send SMS
    messagebird.messages.create(params, function(err, response) {
        if (err) {
            return console.log(err);
        }
        console.log(response);
    });
}

//Export methods
module.exports = {
    sendSMS
};