const jwt = require("jsonwebtoken");
const User = require('../models/userModel');
const Kid = require('../models/userKidModel');
const key = require('../secrets/authKey.json');
const validator = require('email-validator');
const { checkObj } = require('../public/objChecker');
const bcrypt = require('bcrypt');
const { sendSMS } = require('../Auth/twoFactorAuth');
//Generate randomized strings
const randomize = require('randomatic');

//Sign in the user and return a token
const loginUser = (req, res) => {
    let loginUser = new User();
    loginUser.email = req.body.email;
    loginUser.password = req.body.password;
    //New verification code
    let newCode = randomize('A0', 6);

    //Validate email format
    if (!validator.validate(loginUser.email)) {
        console.log('Invalid email');
        return res.status(422).json({ message: 'Invalid email format' });
    };
    //Check for empty or null fields
    if (checkObj(loginUser)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    //authenticate(User, user.email, user.password, res);
    User.findOne({ email: loginUser.email }, function(err, user) {
        if (!user) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        }
        bcrypt.compare(loginUser.password, user.password, function(err, result) {
            if (!result) {
                // if result false send error
                console.log('Wrong password or email.');
                return res.status(422).json({ message: 'The email or the password are wrong.' });
            }
            //Check if user account is already verified
            if (user.verified !== 'verified') {
                return res.status(422).json({
                    message: 'unverified'
                });
            };
            //Update verification code
            User.findByIdAndUpdate(user._id, { verificationCode: newCode }, function(err) {
                if (err) {
                    console.log(err);
                };
            });
            res.status(201).json({
                id: user._id
            });
            //Send code via sms
            sendSMS(user.phoneNumber, newCode);
        });
    });
};

//Sign in the kid and return a token
const loginKid = (req, res) => {
    let loginKid = new Kid();
    loginKid.userName = req.body.userName;
    loginKid.pin = req.body.pin;

    //Check for empty or null fields
    if (checkObj(loginKid)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    //authenticate(User, user.userName, user.pin, res);
    Kid.findOne({ userName: loginKid.userName }, function(err, user) {
        if (!user) {
            console.log('User not found');
            return res.status(422).json({ message: 'User not found' });
        }
        bcrypt.compare(loginKid.pin, user.pin, function(err, result) {
            if (!result) {
                // if result false send error
                console.log('Wrong password or username.');
                return res.status(422).json({ message: 'The username or the pin are wrong.' });
            }
            // If result true send JWT
            jwt.sign({
                userId: user._id,
                name: user.fullName,
                parentId: user.parentId
            }, key['AUTH_KEY'], (err, token) => {
                res.status(201);
                res.json({
                    id: user._id,
                    name: user.fullName,
                    parentId: user.parentId,
                    token: token
                });
            });
        });
    });
};

// FORMAT OF TOKEN
// Authorization: Bearer <access_token>

// Verify Token
function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        try {
            jwt.verify(req.token, key['AUTH_KEY'], (err, authData) => {
                if (err || !authData) {
                    res.sendStatus(403);
                }
                next();
            });
        } catch (error) {
            next();
        }
    } else {
        // Forbidden
        res.sendStatus(403);
    };
};

//Verify user account with code sended via e-mail
function verifyEmail(req, res) {
    User.findOne({ _id: req.params.id }, function(err, user) {
        if (err || !user) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        }
        if (user.verificationCode !== req.body.code) {
            console.log('Incorrect code', err);
            return res.status(422).json({ message: 'Incorrect code' });
        };
        User.findByIdAndUpdate(req.params.id, { verified: 'verified' }, function(err) {
            if (err) {
                console.log(err);
            };
            res.status(200);
            res.json({
                message: 'User verification successful.'
            });
        });
    });
};

// Get user by id from the db
const verifyAcc = (req, res) => {
    User.findOne({ email: req.body.email }, function(err, user) {
        if (!user) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        }
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (!result) {
                // if result false send error
                console.log('Wrong password or email.');
                return res.status(422).json({ message: 'The email or the password are wrong.' });
            }
            res.status(201).json({
                id: user._id
            });
        });
    });
};

//Verify code sended via SMS
function verifySMS(req, res) {
    User.findOne({ _id: req.params.id }, function(err, user) {
        if (err || !user) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        };
        if (user.verificationCode !== req.body.code) {
            console.log('Incorrect code', err);
            return res.status(422).json({ message: 'Incorrect code' });
        };
        // If result true send JWT
        jwt.sign({
            userId: user._id,
            name: user.name,
            lastname: user.lastname
        }, key['AUTH_KEY'], (err, token) => {
            res.status(201);
            res.json({
                id: user._id,
                name: user.name,
                lastname: user.lastname,
                token: token
            });
        });
    });
};

//Export methods
module.exports = {
    loginUser,
    loginKid,
    verifyToken,
    verifyEmail,
    verifySMS,
    verifyAcc
};