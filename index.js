const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require("cors");
//Import user methods
const {
    getUser,
    postUser,
    putUser,
    deleteUser
} = require('./controllers/userController');
//Import kid methods
const {
    getKid,
    getKidsbyparentId,
    postUserKid,
    putUserKid,
    deleteUserKid,
    delKidByParent
} = require('./controllers/userKidController');
//Import video methods
const {
    getVideo,
    getVideoByFilter,
    getVideosbyUserId,
    postVideo,
    putVideo,
    deleteVideo,
    delVideoByUser
} = require('./controllers/videoController');
//Import login methods
const {
    loginUser,
    loginKid,
    verifyToken,
    verifyEmail,
    verifySMS,
    verifyAcc
} = require('./Auth/login');

const app = express();

// check for cors
app.use(cors());

// Connect to local db
mongoose.connect('mongodb://localhost/tubeKidsDb', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});

let db = mongoose.connection;

// Check connection
db.once('open', function() {
    console.log('Connected to MongoDB');
});

// Check for db errors
db.on('error', function(err) {
    console.log(err);
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// user routes
app.get('/user/:id', verifyToken, getUser);
app.post('/user/add', postUser);
app.post('/user/edit/:id', verifyToken, putUser);
app.delete('/user/:id', verifyToken, deleteUser);

// UserKid routes
app.get('/kid/:id', verifyToken, getKid);
app.get('/kid/byparent/:id', verifyToken, getKidsbyparentId);
app.post('/kid/add', verifyToken, postUserKid);
app.post('/kid/edit/:id', verifyToken, putUserKid);
app.delete('/kid/:id', verifyToken, deleteUserKid);
app.delete('/kid/byParent/:id', verifyToken, delKidByParent);

// Video routes
app.get('/video/:id', verifyToken, getVideo);
app.post('/video/filter/:id', verifyToken, getVideoByFilter);
app.get('/video/byuser/:id', verifyToken, getVideosbyUserId);
app.post('/video/add', verifyToken, postVideo);
app.post('/video/edit/:id', verifyToken, putVideo);
app.delete('/video/:id', verifyToken, deleteVideo);
app.delete('/video/byUser/:id', verifyToken, delVideoByUser);

//Login route
app.post('/user/login', loginUser);
app.post('/kid/login', loginKid);
app.post('/user/verify/:id', verifyEmail);
app.post('/user/authenticate/:id', verifySMS);
app.post('/user/verifyAcc', verifyAcc)

// handle 404
app.use(function(req, res, next) {
    res.status(404);
    res.send({
        error: "Not found"
    });
    return;
});

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));