let mongoose = require('mongoose');

// Video schema
let videoSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    url: {
        type: String,
        require: true
    },
    userId: {
        type: String
    }
});

let Video = module.exports = mongoose.model('Video', videoSchema);