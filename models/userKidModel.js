let mongoose = require('mongoose');

// userKid schema
let userKidSchema = mongoose.Schema({
    fullName: {
        type: String,
        require: true
    },
    userName: {
        type: String,
        require: true
    },
    // Pin must have 6 digits
    pin: {
        type: String,
        require: true
    },
    age: {
        type: Number
    },
    parentId: {
        type: String,
        require: true
    }
});

let UserKid = module.exports = mongoose.model('UserKid', userKidSchema, 'kids');