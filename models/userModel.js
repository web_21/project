let mongoose = require('mongoose');

// User schema
let userSchema = mongoose.Schema({
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    // user must repeat the password to verify
    name: {
        type: String,
        require: true
    },
    lastname: {
        type: String,
        require: true
    },
    country: {
        type: String,
        require: true
    },
    // format mm/dd/yyyy
    birthday: {
        type: String,
        require: true
    },
    phoneNumber: {
        type: Number,
        require: true
    },
    verified: {
        type: String
    },
    verificationCode: {
        type: String
    }
});

let User = module.exports = mongoose.model('User', userSchema);