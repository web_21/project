const UserKid = require('../models/userKidModel');
const { checkObj, pinLength } = require('../public/objChecker');
const bcrypt = require('bcrypt');

// Get kid by id from db
const getKid = (req, res) => {
    UserKid.findOne({ _id: req.params.id }, function(err, kid) {
        if (err || !kid) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        };
        res.status(201);
        res.header({ 'location': `http://localhost/kid/${kid.id}` });
        res.json(kid);
    });
};

//Get kids by parent Id
const getKidsbyparentId = (req, res) => {
    UserKid.find({ parentId: req.params.id }, function(err, kids) {
        if (err || !kids) {
            console.log('No kids found', err);
            return res.status(422).json({ message: 'No kids found' });
        };
        res.status(201);
        res.json(kids);
    });
};

// Add submit POST
const postUserKid = (req, res) => {
    let userKid = new UserKid();
    userKid.fullName = req.body.fullName;
    userKid.userName = req.body.userName;
    userKid.pin = req.body.pin;
    userKid.age = req.body.age;
    userKid.parentId = req.body.parentId;

    if (checkObj(userKid)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!pinLength(userKid.pin, 6)) {
        console.log('Invalid pin length');
        return res.status(422).json({ message: 'Pin must be 6 digits long' });
    };
    //hashing a password before saving it to the database
    bcrypt.hash(userKid.pin, 10, function(err, hash) {
        userKid.pin = hash;
        //Save the user
        userKid.save(function(err) {
            if (err) {
                res.status(422);
                console.log('Error while saving the user', err);
                res.json({ message: 'There was an error saving the user' });
            };
            res.status(201);
            res.header({ 'location': `http://localhost/kid/${userKid.id}` });
            res.json(userKid);
        });
    });
};

// Update submit POST
const putUserKid = (req, res) => {
    let userKid = {};
    userKid.fullName = req.body.fullName;
    userKid.userName = req.body.userName;
    userKid.pin = req.body.pin;
    userKid.age = req.body.age;

    checkPass = req.body.pin.match(/(\$2b\$10)/);

    if (checkObj(userKid)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!checkPass) {
        if (!pinLength(userKid.pin, 6)) {
            console.log('Invalid pin length');
            return res.status(422).json({ message: 'Pin must be 6 digits long' });
        };
    }
    bcrypt.hash(userKid.pin, 10, function(err, hash) {
        if (!checkPass) {
            userKid.pin = hash;
        }
        //Save the user
        UserKid.updateOne({ _id: req.params.id }, userKid, function(err, data) {
            if (err || !data) {
                res.status(422);
                console.log('Error while saving changes', err);
                res.json({ message: 'There was an error saving the changes' });
            };
            res.status(200);
            res.header({ 'location': `http://localhost/kid/${userKid.id}` });
            res.json(userKid);
        });
    });
};

// Delete a kid
const deleteUserKid = (req, res) => {
    UserKid.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err || !data) {
            res.status(422);
            console.log('Error while deleting user', err);
            res.json({ message: 'There was an error deleting the user' });
        }
        res.status(204);
        res.header({ 'location': `http://localhost/kid` });
        res.json();
    });
};

// Delete all kids by parent Id
const delKidByParent = (req, res) => {
    UserKid.deleteMany({ parentId: req.params.id }, function(err, data) {
        if (err || !data) {
            res.status(422);
            console.log('Error while deleting user', err);
            res.json({ message: 'There was an error deleting the user' });
        }
        res.status(204);
        res.header({ 'location': `http://localhost/kid` });
        res.json();
    });
};

// export methods
module.exports = {
    getKid,
    getKidsbyparentId,
    postUserKid,
    putUserKid,
    deleteUserKid,
    delKidByParent
};