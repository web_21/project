const User = require('../models/userModel');
const validator = require('email-validator');
const { checkObj, verifyAge } = require('../public/objChecker');
const bcrypt = require('bcrypt');
const Key = require('../secrets/sendgrid.json');

// using Twilio SendGrid's v3 Node.js Library
const sgMail = require('@sendgrid/mail');

//Generate randomized strings
const randomize = require('randomatic');

// Get user by id from the db
const getUser = (req, res) => {
    User.findOne({ _id: req.params.id }, function(err, user) {
        if (err || !user) {
            console.log('User not found', err);
            return res.status(422).json({ message: 'User not found' });
        }
        res.status(201);
        res.header({ 'location': `http://localhost/user/${user.id}` });
        res.json(user);
    });
};

// Add submit POST
const postUser = (req, res) => {
    let user = new User();
    user.email = req.body.email;
    user.password = req.body.password;
    user.name = req.body.name;
    user.lastname = req.body.lastname;
    user.country = req.body.country;
    user.birthday = req.body.birthday;
    user.phoneNumber = req.body.phoneNumber;
    user.verified = 'unverified';
    user.verificationCode = randomize('A0', 6);

    if (checkObj(user)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!verifyAge(user.birthday)) {
        console.log('User must be 18+');
        return res.status(422).json({ message: 'You must be 18 or older to create an account.' });
    }
    if (!validator.validate(user.email)) {
        console.log('Invalid email');
        return res.status(422).json({ message: 'Invalid email format' });
    };
    //hashing a password before saving it to the database
    bcrypt.hash(user.password, 10, function(err, hash) {
        user.password = hash;
        //Save the user
        user.save(function(err) {
            if (err) {
                res.status(422);
                console.log('Error while saving changes', err);
                res.json({ message: 'There was an error saving the changes' });
            };
            res.status(201);
            res.header({ 'location': `http://localhost/user/${user.id}` });
            res.json({ id: user.id });
            sendVerificationEmail(user.email, user.name, user.verificationCode);
        });
    });
};

// Update submit POST
const putUser = (req, res) => {
    let user = {};
    user.email = req.body.email;
    user.password = req.body.password;
    user.name = req.body.name;
    user.lastname = req.body.lastname;
    user.country = req.body.country;
    user.birthday = req.body.birthday;
    user.phoneNumber = req.body.phoneNumber;

    checkPass = req.body.password.match(/(\$2b\$10)/);

    if (checkObj(user)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!validator.validate(user.email)) {
        console.log('Invalid email');
        return res.status(422).json({ message: 'Invalid email format' });
    };
    bcrypt.hash(user.password, 10, function(err, hash) {
        if (!checkPass) {
            user.password = hash;
        }
        User.updateOne({ _id: req.params.id }, user, function(err, data) {
            if (err || !data) {
                res.status(422);
                console.log('Error while saving changes', err);
                res.json({ message: 'There was an error saving the changes' });
            };
            res.status(200);
            res.header({ 'location': `http://localhost/user/${user.id}` });
            res.json({
                message: 'User information updated.'
            });
        });
    });
};

// Delete a user
const deleteUser = (req, res) => {
    User.deleteOne({ _id: req.params.id }, function(err, user) {
        if (err || !user) {
            console.log('Error while deleting user');
            return res.status(422).json({ message: 'There was an error deleting the user' });
        }
        res.status(204);
        res.header({ 'location': `http://localhost/user` });
        res.json();
    });
};

const sendVerificationEmail = (toEmail, name, code) => {
    //Set APikey
    //sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    sgMail.setApiKey(Key['SENDGRID_API_KEY']);
    //E-mail template
    const msg = {
        to: toEmail,
        from: 'magm1581@gmail.com',
        subject: 'Verification code',
        html: `<h3>Hi ${name} welcome to TubeKids, please use this code to verify your account:</h3> 
        <h2><strong>${code}</strong></h2>`
    };
    sgMail.send(msg).catch((err) => {
        console.log(err);
    });
};

// export methods
module.exports = {
    getUser,
    postUser,
    putUser,
    deleteUser
};