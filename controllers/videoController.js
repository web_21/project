const Video = require('../models/videoModel');
const youtubeUrl = require('youtube-url');
const { checkObj } = require('../public/objChecker');

// Filter videos by name
const getVideoByFilter = (req, res) => {
    //Using regex to match the results
    Video.find({ name: new RegExp(req.body.filter, 'i'), userId: req.params.id }, function(err, matches) {
        if (err || !matches) {
            console.log('No matches', err);
            return res.status(422).json({ message: 'No matches found' });
        };
        res.status(201);
        res.json(matches);
    });
};

//Get videos by userId
const getVideosbyUserId = (req, res) => {
    Video.find({ userId: req.params.id }, function(err, videos) {
        if (err || !videos) {
            console.log('No matches', err);
            return res.status(422).json({ message: 'No matches found' });
        };
        res.status(201).json(videos);
    });
};

//Get video by id
const getVideo = (req, res) => {
    Video.findOne({ _id: req.params.id }, function(err, video) {
        if (err || !video) {
            console.log('No matches', err);
            return res.status(422).json({ message: 'No matches found' });
        };
        res.status(201).json(video);
    });
};

// Add submit POST
const postVideo = (req, res) => {
    let video = new Video();
    video.name = req.body.name;
    video.url = 'https://www.youtube.com/embed/';
    video.userId = req.body.userId;

    videoId = req.body.url.match(/v=(\w+-?\w+)&?/);
    video.url = video.url + videoId[1];

    if (checkObj(video)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!youtubeUrl.valid(video.url)) {
        console.log('Invalid youtube url');
        return res.status(422).json({ message: 'You must enter a Youtube URL' });
    };
    video.save(function(err) {
        if (err) {
            res.status(422);
            console.log('Error while saving the video', err);
            res.json({ message: 'There was an error saving the video' });
        };
        res.status(201);
        res.header({ 'location': `http://localhost/videos/${video.id}` });
        res.json(video);
    });
};

// Update submit POST
const putVideo = (req, res) => {
    let video = {};
    video.name = req.body.name;
    video.url = req.body.url;

    if (checkObj(video)) {
        console.log('Null or empty fields');
        return res.status(422).json({ message: 'All fields must be completed' });
    };
    if (!youtubeUrl.valid(video.url)) {
        console.log('Invalid youtube url');
        return res.status(422).json({ message: 'You must enter a Youtube URL' });
    };
    Video.updateOne({ _id: req.params.id }, video, function(err, data) {
        console.log(data);
        if (err || !data) {
            res.status(422);
            console.log('Error while saving changes', err);
            res.json({ message: 'There was an error saving the changes' });
        };
        res.status(200);
        res.header({ 'location': `http://localhost/videos/${video.id}` });
        res.json(video);
    });
};

// Delete a video
const deleteVideo = (req, res) => {
    Video.deleteOne({ _id: req.params.id }, function(err, data) {
        if (err || !data) {
            res.status(422);
            console.log('Error while deleting video', err);
            res.json({ message: 'There was an error deleting the video' });
        }
        res.status(204);
        res.header({
            'location': `http://localhost/videos`
        });
        res.json();
    });
};

// Delete all videos by userId
const delVideoByUser = (req, res) => {
    Video.deleteMany({ userId: req.params.id }, function(err, data) {
        if (err || !data) {
            res.status(422);
            console.log('Error while deleting video', err);
            res.json({ message: 'There was an error deleting the video' });
        }
        res.status(204);
        res.header({
            'location': `http://localhost/videos`
        });
        res.json();
    });
};

// export methods
module.exports = {
    getVideo,
    getVideoByFilter,
    getVideosbyUserId,
    postVideo,
    putVideo,
    deleteVideo,
    delVideoByUser
};